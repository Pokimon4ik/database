﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class FormObjs : Form
    {
        public FormObjs()
        {
            InitializeComponent();
        }

        private void объектBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.объектBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.bond_artDataSet);

        }

        private void FormObjs_Load(object sender, EventArgs e)
        {
            string sql = "SELECT * FROM Объект";
            using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
            {
                connection.Open();
                var command = connection.CreateCommand();

                command.CommandText = sql;

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var lvi = new ListViewItem(reader["Код объекта"].ToString());
                        lvi.SubItems.Add(reader["Наименование объекта"].ToString());
                        lvi.SubItems.Add(reader["Тип объекта"].ToString());
                        lvi.SubItems.Add(reader["Вес"].ToString());
                        lvi.SubItems.Add(reader["Цена"].ToString());
                        lvObjs.Items.Add(lvi);
                    }
                }
            }
        }

        private void FormObjs_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            NewObj jojo = new NewObj();
            jojo.Owner = this;
            jojo.Show();
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormModsToObjs jojo = new FormModsToObjs();
            jojo.Owner = this;
            Owner.Hide();
            jojo.Show();
        }
    }
}
