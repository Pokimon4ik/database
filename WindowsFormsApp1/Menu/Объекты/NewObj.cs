﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class NewObj : Form
    {
        Dictionary<int, string> adres = new Dictionary<int, string>(); 

        public NewObj()
        {
            InitializeComponent();
        }
        

        private void NewObj_Load(object sender, EventArgs e)
        {
            string sql;

            sql = "SELECT Адрес,[Код пункта] FROM [Пункт проката]";
            using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
            {
                connection.Open();
                var command = connection.CreateCommand();

                command.CommandText = sql;

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        cbPoints.Items.Add(reader["Адрес"]);
                        adres.Add((int)reader["Код пункта"], reader["Адрес"].ToString());
                    }
                }
            }


        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            FormObjs foo = this.Owner as FormObjs;

            using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
            {
                connection.Open();
                #region Добавление в объекты
                string sqlExpression = "INSERT INTO Объект ([Наименование объекта], Цена, [Тип объекта], Вес, [Код пункта]) VALUES (@Name, @Price, @Type, @Weight, @IdPoint)";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                SqlParameter Name = new SqlParameter("@Name", tbName.Text);
                command.Parameters.Add(Name);

                SqlParameter Price = new SqlParameter("@Price", tbPrice.Text);
                command.Parameters.Add(Price);

                SqlParameter Type = new SqlParameter("@Type", tbType.Text);
                command.Parameters.Add(Type);

                SqlParameter Weight = new SqlParameter("@Weight", tbWeight.Text);
                command.Parameters.Add(Weight);

                int key = adres.FirstOrDefault(x => x.Value == cbPoints.Text).Key;

                SqlParameter Point = new SqlParameter("@IdPoint", key);
                command.Parameters.Add(Point);

                command.ExecuteNonQuery();
                #endregion

                #region Обновление таблици
                foo.lvObjs.Items.Clear();

                string sql = "SELECT * FROM Объект";
            
                command = connection.CreateCommand();

                command.CommandText = sql;

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var lvi = new ListViewItem(reader["Код объекта"].ToString());
                        lvi.SubItems.Add(reader["Наименование объекта"].ToString());
                        lvi.SubItems.Add(reader["Тип объекта"].ToString());
                        lvi.SubItems.Add(reader["Вес"].ToString());
                        lvi.SubItems.Add(reader["Цена"].ToString());
                        foo.lvObjs.Items.Add(lvi);
                    }
                }
                #endregion
            }
            Close();
        }
    }
}
