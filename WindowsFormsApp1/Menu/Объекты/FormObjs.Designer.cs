﻿namespace WindowsFormsApp1
{
    partial class FormObjs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lvObjs = new System.Windows.Forms.ListView();
            this.ID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.NameObj = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Type = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Wight = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Price = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bond_artDataSet = new WindowsFormsApp1.bond_artDataSet();
            this.объектBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.объектTableAdapter = new WindowsFormsApp1.bond_artDataSetTableAdapters.ОбъектTableAdapter();
            this.tableAdapterManager = new WindowsFormsApp1.bond_artDataSetTableAdapters.TableAdapterManager();
            this.button1 = new System.Windows.Forms.Button();
            this.btnDelete1 = new WindowsFormsApp1.BtnDelete();
            this.Exit = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.bond_artDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.объектBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // lvObjs
            // 
            this.lvObjs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ID,
            this.NameObj,
            this.Type,
            this.Wight,
            this.Price});
            this.lvObjs.Location = new System.Drawing.Point(12, 12);
            this.lvObjs.Name = "lvObjs";
            this.lvObjs.Size = new System.Drawing.Size(414, 277);
            this.lvObjs.TabIndex = 1;
            this.lvObjs.UseCompatibleStateImageBehavior = false;
            this.lvObjs.View = System.Windows.Forms.View.Details;
            // 
            // ID
            // 
            this.ID.Text = "Код объекта";
            this.ID.Width = 103;
            // 
            // NameObj
            // 
            this.NameObj.Text = "Название";
            this.NameObj.Width = 102;
            // 
            // Type
            // 
            this.Type.Text = "Тип объекта";
            this.Type.Width = 90;
            // 
            // Wight
            // 
            this.Wight.Text = "Вес";
            this.Wight.Width = 39;
            // 
            // Price
            // 
            this.Price.Text = "Цена";
            this.Price.Width = 80;
            // 
            // bond_artDataSet
            // 
            this.bond_artDataSet.DataSetName = "bond_artDataSet";
            this.bond_artDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // объектBindingSource
            // 
            this.объектBindingSource.DataMember = "Объект";
            this.объектBindingSource.DataSource = this.bond_artDataSet;
            // 
            // объектTableAdapter
            // 
            this.объектTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.UpdateOrder = WindowsFormsApp1.bond_artDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.Бланк_регистрацииTableAdapter = null;
            this.tableAdapterManager.Время_доставкиTableAdapter = null;
            this.tableAdapterManager.КлиентTableAdapter = null;
            this.tableAdapterManager.Модификации_ОбъектаTableAdapter = null;
            this.tableAdapterManager.МодификацияTableAdapter = null;
            this.tableAdapterManager.ОбъектTableAdapter = this.объектTableAdapter;
            this.tableAdapterManager.Пункт_прокатаTableAdapter = null;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 324);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(148, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Добавить новый объект";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnDelete1
            // 
            this.btnDelete1.Location = new System.Drawing.Point(166, 321);
            this.btnDelete1.Name = "btnDelete1";
            this.btnDelete1.Size = new System.Drawing.Size(84, 31);
            this.btnDelete1.TabIndex = 3;
            // 
            // Exit
            // 
            this.Exit.Location = new System.Drawing.Point(256, 324);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(170, 23);
            this.Exit.TabIndex = 4;
            this.Exit.Text = "Выход";
            this.Exit.UseVisualStyleBackColor = true;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 295);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(414, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Добавить модификацию к объекту";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // FormObjs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(438, 354);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.Exit);
            this.Controls.Add(this.btnDelete1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lvObjs);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormObjs";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Объекты";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormObjs_FormClosed);
            this.Load += new System.EventHandler(this.FormObjs_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bond_artDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.объектBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private bond_artDataSet bond_artDataSet;
        private System.Windows.Forms.BindingSource объектBindingSource;
        private bond_artDataSetTableAdapters.ОбъектTableAdapter объектTableAdapter;
        private bond_artDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.ColumnHeader ID;
        private System.Windows.Forms.ColumnHeader NameObj;
        private System.Windows.Forms.ColumnHeader Type;
        private System.Windows.Forms.ColumnHeader Wight;
        private System.Windows.Forms.ColumnHeader Price;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.ListView lvObjs;
        private BtnDelete btnDelete1;
        private System.Windows.Forms.Button Exit;
        private System.Windows.Forms.Button button2;
    }
}