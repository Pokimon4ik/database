﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class FormBlankNewClients : Form
    {

        public FormBlankNewClients()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "bond_artDataSet.Бланк_регистрации". При необходимости она может быть перемещена или удалена.
            this.бланк_регистрацииTableAdapter.Fill(this.bond_artDataSet.Бланк_регистрации);
            TextBoxData.Text = DateTime.Now.ToString();

            string sql;

            sql = "SELECT [Наименование объекта],Цена FROM Объект";
            using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
            {
                connection.Open();
                var command = connection.CreateCommand();

                command.CommandText = sql;

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        cbObjs.Items.Add(reader["Наименование объекта"] + " - " + reader["Цена"]);
                    }
                }
            }


            sql = "SELECT Адрес FROM [Пункт проката]";
            using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
            {
                connection.Open();
                var command = connection.CreateCommand();

                command.CommandText = sql;

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        cbPoints.Items.Add(reader["Адрес"]);
                    }
                }
            }

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        private void Vnesti_Click(object sender, EventArgs e)
        {

            using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
            {
                connection.Open();

                #region Проверка на существующего клиента
                string idClient = "";
                bool existence = false;
                string sql;
                sql = "SELECT * FROM Клиент";
                var command = connection.CreateCommand();
                command.CommandText = sql;
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        if (textBox_FIO == reader["ФИО"] && textBox_Adres == reader["Адрес"] && textBox_Telefon == reader["Телефон"])
                        {
                            idClient = reader["Код клиента"].ToString();
                            existence = true;
                        }
                    }
                }
                #endregion

                #region Добавление в клиенты
                if (!existence)
                {
                    sql = "INSERT INTO Клиент (ФИО, Адрес, Телефон) VALUES (@FIO, @Adres, @Telefon);SET @id=SCOPE_IDENTITY()";

                    command = new SqlCommand(sql, connection);

                    SqlParameter FioParam = new SqlParameter("@FIO", textBox_FIO.Text);
                    command.Parameters.Add(FioParam);

                    SqlParameter AdresParam = new SqlParameter("@Adres", textBox_Adres.Text);
                    command.Parameters.Add(AdresParam);

                    SqlParameter TelefonParam = new SqlParameter("@Telefon", textBox_Telefon.Text);
                    command.Parameters.Add(TelefonParam);

                    SqlParameter idParam = new SqlParameter
                    {
                        ParameterName = "@id",
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Output // параметр выходной
                    };
                    command.Parameters.Add(idParam);
                    command.ExecuteNonQuery();
                    idClient = idParam.Value.ToString();
                }
                #endregion

                #region Вычисление ID
                sql = $"SELECT [Код объекта],[Цена] FROM Объект WHERE [Наименование объекта] = '{cbObjs.Text.Split()[0]}'";
                command = connection.CreateCommand();

                command.CommandText = sql;

                string idObj = "";
                string price = "";

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        idObj = reader["Код объекта"].ToString();
                        price = reader["Цена"].ToString();
                        price = price.Remove(price.Length - 4);
                    }
                }


                sql = $"SELECT [Код пункта] FROM [Пункт проката] WHERE [Адрес] = '{cbPoints.Text}'";
                command = connection.CreateCommand();

                command.CommandText = sql;

                string idDes = "";

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        idDes = reader["Код пункта"].ToString();
                    }
                }
                #endregion

                #region Добавление в блакн
                sql = string.Format($"INSERT INTO [Бланк регистрации] ([Код клиента],[Код объекта],[Код Пункта],Дата,[Время начала аренды],[Время конца аренды],[Цена аренды]) VALUES (@IDClient, @IDObj, @IDdes,@Date, @DateNow, @DateNext,@Price)");
                command = new SqlCommand(sql, connection);

                SqlParameter IDClient = new SqlParameter("@IDClient", idClient);
                command.Parameters.Add(IDClient);

                SqlParameter IDObj = new SqlParameter("@IDObj", idObj);
                command.Parameters.Add(IDObj);

                SqlParameter IDdes = new SqlParameter("@IDdes", idDes);
                command.Parameters.Add(IDdes);


                SqlParameter Date = new SqlParameter("@Date", DateTime.Now);
                command.Parameters.Add(Date);


                SqlParameter DateNow = new SqlParameter("@DateNow", dtpNow.Value);
                command.Parameters.Add(DateNow);


                SqlParameter DateNext = new SqlParameter("@DateNext", dtpNow.Value.AddDays((double)numericUpDown1.Value));
                command.Parameters.Add(DateNext);


                SqlParameter Price = new SqlParameter("@Price", price);
                command.Parameters.Add(Price);

                int number = command.ExecuteNonQuery();
                Console.WriteLine("Добавлено объектов: {0}", number);
                #endregion

                #region Проверка нахожднения на складе
                sql = $"SELECT [Код пункта] FROM Объект WHERE [Наименование объекта] = '{cbObjs.Text.Split()[0]}'";
                command = connection.CreateCommand();
                command.CommandText = sql;

                var idDesObj = "";

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        idDesObj = reader["Код пункта"].ToString();
                    }
                }

                if (idDes != idDesObj)
                {
                    List<(string, string, long)> graph = new List<(string, string, long)>();

                    sql = $"SELECT * FROM [Время доставки]";
                    command = connection.CreateCommand();
                    command.CommandText = sql;

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            (string, string, long) jojo1 = (reader["Код пункта назначения"].ToString(), reader["Код пункта выезда"].ToString(), Convert.ToInt64(reader["Время доставки"]));
                            (string, string, long) jojo2 = (reader["Код пункта выезда"].ToString(), reader["Код пункта назначения"].ToString(), Convert.ToInt64(reader["Время доставки"]));
                            graph.Add(jojo1);
                            graph.Add(jojo2);
                        }
                    }

                    Kortej kortej = new Kortej(graph);
                    MessageBox.Show("Товар пребудет через - " + kortej.SmallDistanceTwoPoints(graph, idDes, idDesObj).ToString(),"Время доставки");
                }
                #endregion
            }

            Console.Read();
            textBox_FIO.Text = "";
            textBox_Adres.Text = "";
            textBox_Telefon.Text = "";

            #region Обновление
            FormBlank jojo = (FormBlank)Owner;
            jojo.lvBlank.Items.Clear();

            string sqlE = "SELECT * FROM [Бланк регистрации] " +
            "INNER JOIN Клиент ON Клиент.[Код клиента] = [Бланк регистрации].[Код клиента] " +
            "INNER JOIN [Пункт проката] ON [Пункт проката].[Код пункта] = [Бланк регистрации].[Код пункта] " +
            "INNER JOIN Объект ON Объект.[Код объекта] = [Бланк регистрации].[Код объекта]";
            using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
            {
                connection.Open();
                var command = connection.CreateCommand();

                command.CommandText = sqlE;

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var lvi = new ListViewItem(reader["ФИО"].ToString());
                        lvi.SubItems.Add(reader["Цена аренды"].ToString());
                        lvi.SubItems.Add(reader["Наименование объекта"].ToString());
                        lvi.SubItems.Add(reader["Адрес"].ToString());
                        lvi.SubItems.Add(reader["Время начала аренды"].ToString());
                        lvi.SubItems.Add(reader["Время конца аренды"].ToString());
                        jojo.lvBlank.Items.Add(lvi);
                    }
                }
            } 
            #endregion

        }

        private void TextBox_click(object sender, EventArgs e)
        {

        }

    }
}
