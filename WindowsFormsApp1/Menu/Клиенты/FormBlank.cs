﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class FormBlank : Form
    {
        public FormBlank()
        {
            InitializeComponent();
        }

        private void FormBlank_Load(object sender, EventArgs e)
        {
            string sql = "SELECT * FROM [Бланк регистрации] " +
                "INNER JOIN Клиент ON Клиент.[Код клиента] = [Бланк регистрации].[Код клиента] " +
                "INNER JOIN [Пункт проката] ON [Пункт проката].[Код пункта] = [Бланк регистрации].[Код пункта] " +
                "INNER JOIN Объект ON Объект.[Код объекта] = [Бланк регистрации].[Код объекта]";
            using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
            {
                connection.Open();
                var command = connection.CreateCommand();

                command.CommandText = sql;

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var lvi = new ListViewItem(reader["ФИО"].ToString());
                        lvi.SubItems.Add(reader["Цена аренды"].ToString());
                        lvi.SubItems.Add(reader["Наименование объекта"].ToString());
                        lvi.SubItems.Add(reader["Адрес"].ToString());
                        lvi.SubItems.Add(reader["Время начала аренды"].ToString());
                        lvi.SubItems.Add(reader["Время конца аренды"].ToString());
                        lvBlank.Items.Add(lvi);
                    }
                }
            }
        }

        private void AddNew_Click(object sender, EventArgs e)
        {
            FormBlankNewClients jojo = new FormBlankNewClients();
            jojo.Owner = this;
            jojo.Show();
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FormBlank_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Show();
        }

    }
}
