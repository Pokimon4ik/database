﻿namespace WindowsFormsApp1
{
    partial class FormBlankNewClients
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.Vnesti = new System.Windows.Forms.Button();
            this.TextBoxData = new System.Windows.Forms.TextBox();
            this.textBox_FIO = new System.Windows.Forms.TextBox();
            this.textBox_Adres = new System.Windows.Forms.TextBox();
            this.textBox_Telefon = new System.Windows.Forms.TextBox();
            this.bond_artDataSet = new WindowsFormsApp1.bond_artDataSet();
            this.бланк_регистрацииBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.бланк_регистрацииTableAdapter = new WindowsFormsApp1.bond_artDataSetTableAdapters.Бланк_регистрацииTableAdapter();
            this.tableAdapterManager = new WindowsFormsApp1.bond_artDataSetTableAdapters.TableAdapterManager();
            this.клиентBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.клиентTableAdapter = new WindowsFormsApp1.bond_artDataSetTableAdapters.КлиентTableAdapter();
            this.объектBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.объектTableAdapter = new WindowsFormsApp1.bond_artDataSetTableAdapters.ОбъектTableAdapter();
            this.пунктПрокатаBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.пункт_прокатаTableAdapter = new WindowsFormsApp1.bond_artDataSetTableAdapters.Пункт_прокатаTableAdapter();
            this.cbObjs = new System.Windows.Forms.ComboBox();
            this.dtpNow = new System.Windows.Forms.DateTimePicker();
            this.cbPoints = new System.Windows.Forms.ComboBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bond_artDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.бланк_регистрацииBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.клиентBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.объектBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.пунктПрокатаBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(363, 112);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(56, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Выход";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Vnesti
            // 
            this.Vnesti.Location = new System.Drawing.Point(280, 113);
            this.Vnesti.Name = "Vnesti";
            this.Vnesti.Size = new System.Drawing.Size(77, 23);
            this.Vnesti.TabIndex = 8;
            this.Vnesti.Text = "Внести";
            this.Vnesti.UseVisualStyleBackColor = true;
            this.Vnesti.Click += new System.EventHandler(this.Vnesti_Click);
            // 
            // TextBoxData
            // 
            this.TextBoxData.Location = new System.Drawing.Point(280, 25);
            this.TextBoxData.Name = "TextBoxData";
            this.TextBoxData.ReadOnly = true;
            this.TextBoxData.Size = new System.Drawing.Size(139, 20);
            this.TextBoxData.TabIndex = 10;
            // 
            // textBox_FIO
            // 
            this.textBox_FIO.Location = new System.Drawing.Point(12, 25);
            this.textBox_FIO.Name = "textBox_FIO";
            this.textBox_FIO.Size = new System.Drawing.Size(87, 20);
            this.textBox_FIO.TabIndex = 1;
            this.textBox_FIO.Click += new System.EventHandler(this.TextBox_click);
            // 
            // textBox_Adres
            // 
            this.textBox_Adres.Location = new System.Drawing.Point(12, 70);
            this.textBox_Adres.Name = "textBox_Adres";
            this.textBox_Adres.Size = new System.Drawing.Size(87, 20);
            this.textBox_Adres.TabIndex = 2;
            this.textBox_Adres.Click += new System.EventHandler(this.TextBox_click);
            // 
            // textBox_Telefon
            // 
            this.textBox_Telefon.Location = new System.Drawing.Point(12, 115);
            this.textBox_Telefon.Name = "textBox_Telefon";
            this.textBox_Telefon.Size = new System.Drawing.Size(87, 20);
            this.textBox_Telefon.TabIndex = 3;
            this.textBox_Telefon.Click += new System.EventHandler(this.TextBox_click);
            // 
            // bond_artDataSet
            // 
            this.bond_artDataSet.DataSetName = "bond_artDataSet";
            this.bond_artDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // бланк_регистрацииBindingSource
            // 
            this.бланк_регистрацииBindingSource.DataMember = "Бланк регистрации";
            this.бланк_регистрацииBindingSource.DataSource = this.bond_artDataSet;
            // 
            // бланк_регистрацииTableAdapter
            // 
            this.бланк_регистрацииTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.UpdateOrder = WindowsFormsApp1.bond_artDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.Бланк_регистрацииTableAdapter = this.бланк_регистрацииTableAdapter;
            this.tableAdapterManager.Время_доставкиTableAdapter = null;
            this.tableAdapterManager.КлиентTableAdapter = null;
            this.tableAdapterManager.Модификации_ОбъектаTableAdapter = null;
            this.tableAdapterManager.МодификацияTableAdapter = null;
            this.tableAdapterManager.ОбъектTableAdapter = null;
            this.tableAdapterManager.Пункт_прокатаTableAdapter = null;
            // 
            // клиентBindingSource
            // 
            this.клиентBindingSource.DataMember = "Клиент";
            this.клиентBindingSource.DataSource = this.bond_artDataSet;
            // 
            // клиентTableAdapter
            // 
            this.клиентTableAdapter.ClearBeforeFill = true;
            // 
            // объектBindingSource
            // 
            this.объектBindingSource.DataMember = "Объект";
            this.объектBindingSource.DataSource = this.bond_artDataSet;
            // 
            // объектTableAdapter
            // 
            this.объектTableAdapter.ClearBeforeFill = true;
            // 
            // пунктПрокатаBindingSource
            // 
            this.пунктПрокатаBindingSource.DataMember = "Пункт проката";
            this.пунктПрокатаBindingSource.DataSource = this.bond_artDataSet;
            // 
            // пункт_прокатаTableAdapter
            // 
            this.пункт_прокатаTableAdapter.ClearBeforeFill = true;
            // 
            // cbObjs
            // 
            this.cbObjs.FormattingEnabled = true;
            this.cbObjs.Location = new System.Drawing.Point(118, 70);
            this.cbObjs.Name = "cbObjs";
            this.cbObjs.Size = new System.Drawing.Size(139, 21);
            this.cbObjs.TabIndex = 4;
            // 
            // dtpNow
            // 
            this.dtpNow.Location = new System.Drawing.Point(118, 25);
            this.dtpNow.Name = "dtpNow";
            this.dtpNow.Size = new System.Drawing.Size(139, 20);
            this.dtpNow.TabIndex = 6;
            // 
            // cbPoints
            // 
            this.cbPoints.FormattingEnabled = true;
            this.cbPoints.Location = new System.Drawing.Point(118, 114);
            this.cbPoints.Name = "cbPoints";
            this.cbPoints.Size = new System.Drawing.Size(139, 21);
            this.cbPoints.TabIndex = 5;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(280, 70);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(42, 20);
            this.numericUpDown1.TabIndex = 7;
            this.numericUpDown1.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "ФИО:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(277, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Текущая дата:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(115, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Дата начала аренды:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Адрес:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(115, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(113, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "Арендуемый объект:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(277, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(124, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Длительность аренды:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 99);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "Телефон:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(115, 99);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 13);
            this.label8.TabIndex = 26;
            this.label8.Text = "Пункт проката";
            // 
            // FormBlank
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 154);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.cbPoints);
            this.Controls.Add(this.dtpNow);
            this.Controls.Add(this.cbObjs);
            this.Controls.Add(this.textBox_Telefon);
            this.Controls.Add(this.textBox_Adres);
            this.Controls.Add(this.textBox_FIO);
            this.Controls.Add(this.TextBoxData);
            this.Controls.Add(this.Vnesti);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormBlank";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Бланк";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form2_FormClosed);
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bond_artDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.бланк_регистрацииBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.клиентBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.объектBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.пунктПрокатаBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button Vnesti;
        private System.Windows.Forms.TextBox TextBoxData;
        private System.Windows.Forms.TextBox textBox_FIO;
        private System.Windows.Forms.TextBox textBox_Adres;
        private System.Windows.Forms.TextBox textBox_Telefon;
        private bond_artDataSet bond_artDataSet;
        private System.Windows.Forms.BindingSource бланк_регистрацииBindingSource;
        private bond_artDataSetTableAdapters.Бланк_регистрацииTableAdapter бланк_регистрацииTableAdapter;
        private bond_artDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingSource клиентBindingSource;
        private bond_artDataSetTableAdapters.КлиентTableAdapter клиентTableAdapter;
        private System.Windows.Forms.BindingSource объектBindingSource;
        private bond_artDataSetTableAdapters.ОбъектTableAdapter объектTableAdapter;
        private System.Windows.Forms.BindingSource пунктПрокатаBindingSource;
        private bond_artDataSetTableAdapters.Пункт_прокатаTableAdapter пункт_прокатаTableAdapter;
        private System.Windows.Forms.ComboBox cbObjs;
        private System.Windows.Forms.DateTimePicker dtpNow;
        private System.Windows.Forms.ComboBox cbPoints;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}