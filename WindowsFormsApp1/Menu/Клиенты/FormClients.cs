﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class FormClients : Form
    {
        public FormClients()
        {
            InitializeComponent();
        }
        
        private void Form3_Load(object sender, EventArgs e)
        {
            string sql = "SELECT * FROM Клиент";
            using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
            {
                connection.Open();
                var command = connection.CreateCommand();

                command.CommandText = sql;

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var lvi = new ListViewItem(reader["Код клиента"].ToString());
                        lvi.SubItems.Add(reader["ФИО"].ToString());
                        lvi.SubItems.Add(reader["Адрес"].ToString());
                        lvi.SubItems.Add(reader["Телефон"].ToString());
                        lvClients.Items.Add(lvi);
                    }
                }
            }
        }

        private void Form3_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Show();
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
