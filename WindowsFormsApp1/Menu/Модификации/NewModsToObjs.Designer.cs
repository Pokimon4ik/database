﻿namespace WindowsFormsApp1
{
    partial class NewModsToObjs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbObj = new System.Windows.Forms.ComboBox();
            this.объектBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bond_artDataSet = new WindowsFormsApp1.bond_artDataSet();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.модификацияBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.модификацияTableAdapter = new WindowsFormsApp1.bond_artDataSetTableAdapters.МодификацияTableAdapter();
            this.объектTableAdapter = new WindowsFormsApp1.bond_artDataSetTableAdapters.ОбъектTableAdapter();
            this.clbMods = new System.Windows.Forms.CheckedListBox();
            ((System.ComponentModel.ISupportInitialize)(this.объектBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bond_artDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.модификацияBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Модификации";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Объект";
            // 
            // cbObj
            // 
            this.cbObj.DataSource = this.объектBindingSource;
            this.cbObj.DisplayMember = "Наименование объекта";
            this.cbObj.FormattingEnabled = true;
            this.cbObj.Location = new System.Drawing.Point(12, 25);
            this.cbObj.Name = "cbObj";
            this.cbObj.Size = new System.Drawing.Size(117, 21);
            this.cbObj.TabIndex = 4;
            this.cbObj.ValueMember = "Код объекта";
            this.cbObj.TextChanged += new System.EventHandler(this.cbObj_TextChanged);
            // 
            // объектBindingSource
            // 
            this.объектBindingSource.DataMember = "Объект";
            this.объектBindingSource.DataSource = this.bond_artDataSet;
            // 
            // bond_artDataSet
            // 
            this.bond_artDataSet.DataSetName = "bond_artDataSet";
            this.bond_artDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 301);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Изменить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 330);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(120, 23);
            this.button2.TabIndex = 9;
            this.button2.Text = "Выход";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // модификацияBindingSource
            // 
            this.модификацияBindingSource.DataMember = "Модификация";
            this.модификацияBindingSource.DataSource = this.bond_artDataSet;
            // 
            // модификацияTableAdapter
            // 
            this.модификацияTableAdapter.ClearBeforeFill = true;
            // 
            // объектTableAdapter
            // 
            this.объектTableAdapter.ClearBeforeFill = true;
            // 
            // clbMods
            // 
            this.clbMods.FormattingEnabled = true;
            this.clbMods.Location = new System.Drawing.Point(12, 66);
            this.clbMods.Name = "clbMods";
            this.clbMods.Size = new System.Drawing.Size(120, 229);
            this.clbMods.TabIndex = 10;
            // 
            // NewModsToObjs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(144, 360);
            this.Controls.Add(this.clbMods);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbObj);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "NewModsToObjs";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавление новой связи";
            this.Load += new System.EventHandler(this.NewModsToObjs_Load);
            ((System.ComponentModel.ISupportInitialize)(this.объектBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bond_artDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.модификацияBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbObj;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private bond_artDataSet bond_artDataSet;
        private System.Windows.Forms.BindingSource модификацияBindingSource;
        private bond_artDataSetTableAdapters.МодификацияTableAdapter модификацияTableAdapter;
        private System.Windows.Forms.BindingSource объектBindingSource;
        private bond_artDataSetTableAdapters.ОбъектTableAdapter объектTableAdapter;
        private System.Windows.Forms.CheckedListBox clbMods;
    }
}