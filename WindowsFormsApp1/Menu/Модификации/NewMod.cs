﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class NewMod : Form
    {
        public NewMod()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
            {
                connection.Open();
                #region Добавление в объекты
                string sqlExpression = "INSERT INTO Модификация ([Наименование модификации]) VALUES (@Name)";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                SqlParameter Name = new SqlParameter("@Name", tbMod.Text);
                command.Parameters.Add(Name);

                command.ExecuteNonQuery();
                #endregion
            }

            FormMods jojo = (FormMods)Owner;
            jojo.RefreshVL();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
