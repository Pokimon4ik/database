﻿namespace WindowsFormsApp1
{
    partial class FormMods
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lvMods = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bond_artDataSet = new WindowsFormsApp1.bond_artDataSet();
            this.модификацияBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.модификацияTableAdapter = new WindowsFormsApp1.bond_artDataSetTableAdapters.МодификацияTableAdapter();
            this.tableAdapterManager = new WindowsFormsApp1.bond_artDataSetTableAdapters.TableAdapterManager();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnDelete1 = new WindowsFormsApp1.BtnDelete();
            ((System.ComponentModel.ISupportInitialize)(this.bond_artDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.модификацияBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // lvMods
            // 
            this.lvMods.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.lvMods.Location = new System.Drawing.Point(12, 12);
            this.lvMods.Name = "lvMods";
            this.lvMods.Size = new System.Drawing.Size(204, 157);
            this.lvMods.TabIndex = 0;
            this.lvMods.UseCompatibleStateImageBehavior = false;
            this.lvMods.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Код модификации";
            this.columnHeader1.Width = 105;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Наименование";
            this.columnHeader2.Width = 95;
            // 
            // bond_artDataSet
            // 
            this.bond_artDataSet.DataSetName = "bond_artDataSet";
            this.bond_artDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // модификацияBindingSource
            // 
            this.модификацияBindingSource.DataMember = "Модификация";
            this.модификацияBindingSource.DataSource = this.bond_artDataSet;
            // 
            // модификацияTableAdapter
            // 
            this.модификацияTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.UpdateOrder = WindowsFormsApp1.bond_artDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.Бланк_регистрацииTableAdapter = null;
            this.tableAdapterManager.Время_доставкиTableAdapter = null;
            this.tableAdapterManager.КлиентTableAdapter = null;
            this.tableAdapterManager.Модификации_ОбъектаTableAdapter = null;
            this.tableAdapterManager.МодификацияTableAdapter = this.модификацияTableAdapter;
            this.tableAdapterManager.ОбъектTableAdapter = null;
            this.tableAdapterManager.Пункт_прокатаTableAdapter = null;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(93, 178);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(49, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Выход";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(148, 178);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(68, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Добавить";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnDelete1
            // 
            this.btnDelete1.Location = new System.Drawing.Point(8, 175);
            this.btnDelete1.Name = "btnDelete1";
            this.btnDelete1.Size = new System.Drawing.Size(84, 31);
            this.btnDelete1.TabIndex = 1;
            // 
            // FormMods
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(229, 211);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnDelete1);
            this.Controls.Add(this.lvMods);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormMods";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Модификации";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormMods_FormClosed);
            this.Load += new System.EventHandler(this.FormMods_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bond_artDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.модификацияBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private bond_artDataSet bond_artDataSet;
        private System.Windows.Forms.BindingSource модификацияBindingSource;
        private bond_artDataSetTableAdapters.МодификацияTableAdapter модификацияTableAdapter;
        private bond_artDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        public System.Windows.Forms.ListView lvMods;
        private BtnDelete btnDelete1;
    }
}