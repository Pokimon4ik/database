﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class FormModsToObjs : Form
    {
        public FormModsToObjs()
        {
            InitializeComponent();
        }

        public void RefreshLV()
        {
            lvLink.Items.Clear();

            var sql = "SELECT * FROM [Модификации Объекта] " +
                "INNER JOIN Объект ON Объект.[Код объекта] = [Модификации Объекта].[Код модиф объекта] " +
                "INNER JOIN Модификация ON Модификация.[Код модификации] = [Модификации Объекта].[Код модиф]";
            using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
            {
                connection.Open();
                var command = connection.CreateCommand();

                command.CommandText = sql;

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var lvi = new ListViewItem(reader["Наименование объекта"].ToString());
                        lvi.SubItems.Add(reader["Наименование модификации"].ToString());
                        lvLink.Items.Add(lvi);
                    }
                }
            }
            lvLink.Sort();
        }

        private void FormModsToObjs_Load(object sender, EventArgs e)
        {
            RefreshLV();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FormModsToObjs_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Show();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            NewModsToObjs jojo = new NewModsToObjs();
            jojo.Owner = this;
            jojo.Show();
        }
    }
}
