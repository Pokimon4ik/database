﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class NewModsToObjs : Form
    {
        public NewModsToObjs()
        {
            InitializeComponent();
        }

        Dictionary<string, int> clbAll = new Dictionary<string, int>();

        List<(string, int, bool)> clbSelect = new List<(string, int, bool)>();

        public void PreRefCLB()
        {
            clbSelect.Clear();

            using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
            {
                connection.Open();

                var sql = $"SELECT * FROM Модификация " +
                    $"INNER JOIN [Модификации Объекта] ON [Модификации Объекта].[Код модиф] = Модификация.[Код модификации] " +
                    $"WHERE [Код модиф объекта] = '{cbObj.SelectedValue}'";
                var command = connection.CreateCommand();
                command.CommandText = sql;
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        clbSelect.Add((reader["Наименование модификации"].ToString(), (int)reader["Код модификации"], true));
                    }
                }
            }
        }

        public void RefCLB()
        {
            clbMods.Items.Clear();
            clbAll.Clear();

            using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
            {
                connection.Open();

                var sql = "SELECT * FROM Модификация";
                var command = connection.CreateCommand();

                command.CommandText = sql;
                int count = 0;
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        clbMods.Items.Add(reader["Наименование модификации"]);
                        clbAll.Add(reader["Наименование модификации"].ToString(), (int)reader["Код модификации"]);
                        foreach (var item in clbSelect)
                        {
                            if ((int)reader["Код модификации"] == item.Item2)
                            {
                                clbMods.SetItemChecked(count, true);
                            }
                        }
                        count++;
                    }
                }
            }
        }

        private void NewModsToObjs_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "bond_artDataSet.Объект". При необходимости она может быть перемещена или удалена.
            this.объектTableAdapter.Fill(this.bond_artDataSet.Объект);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "bond_artDataSet.Модификация". При необходимости она может быть перемещена или удалена.
            this.модификацияTableAdapter.Fill(this.bond_artDataSet.Модификация);

            PreRefCLB();
            RefCLB();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
            {
                connection.Open();

                #region Проверка на дублирование
                var sql = $"DELETE FROM [Модификации Объекта] WHERE [Код модиф объекта] = '{cbObj.SelectedValue}'";
                var command = connection.CreateCommand();
                command.CommandText = sql;
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (SqlException)
                {
                    MessageBox.Show("Данные присутствуют в других таблицах", "Ошибка", MessageBoxButtons.OK);
                }
                #endregion

                #region Добавление в объекты
                foreach (var i in clbMods.CheckedItems)
                    {
                        string sqlExpression = "INSERT INTO [Модификации Объекта] ([Код модиф объекта],[Код модиф]) VALUES (@Объект,@Модиф)";
                        command = new SqlCommand(sqlExpression, connection);

                        int tem = -1;
                        clbAll.TryGetValue(i.ToString(), out tem);

                        command.Parameters.AddWithValue("@Объект", cbObj.SelectedValue);
                        command.Parameters.AddWithValue("@Модиф", tem);
                        command.ExecuteNonQuery();
                    }
                #endregion
            }

            FormModsToObjs jojo = (FormModsToObjs)Owner;
            jojo.RefreshLV();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void cbObj_TextChanged(object sender, EventArgs e)
        {
            PreRefCLB();
            RefCLB();
        }
    }
}
