﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1.Menu.Пункты
{
    public partial class NewLinkClass : Form
    {
        public NewLinkClass()
        {
            InitializeComponent();
        }

        private void NewLinkClass_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "bond_artDataSet.Пункт_проката". При необходимости она может быть перемещена или удалена.
            this.пункт_прокатаTableAdapter.Fill(this.bond_artDataSet.Пункт_проката);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox2.Text == comboBox1.Text)
            {
                MessageBox.Show("Связь невозможна", "Ошибка");
            }
            else
            {
                using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
                {
                    connection.Open();

                    bool temp = false;

                    string sql = "SELECT * FROM [Время доставки]";
                    var com = connection.CreateCommand();
                    com.CommandText = sql;
                    using (var reader = com.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader["Код пункта назначения"] == comboBox2.SelectedValue && reader["Код пункта выезда"] == comboBox1.SelectedValue)
                            {
                                temp = true;
                                break;
                            }
                        }
                    }

                    #region Добавление
                    if (!temp)
                    {
                        var sqlExpression = "INSERT INTO [Время доставки] ([Код пункта выезда],[Код пункта назначения],[Время доставки]) VALUES (@Выезд,@Назначение,@Время)";
                        var command = new SqlCommand(sqlExpression, connection);
                        command.Parameters.AddWithValue("@Выезд", comboBox1.SelectedValue);
                        command.Parameters.AddWithValue("@Назначение", comboBox2.SelectedValue);
                        command.Parameters.AddWithValue("@Время", numericUpDown1.Value);
                        command.ExecuteNonQuery();
                    }
                    else
                    {
                        MessageBox.Show("Такая связь уже существует", "Ошибка");
                    }
                    #endregion
                }

                LinkPoints jojo = (LinkPoints)Owner;
                jojo.RefLV();
            }
        }
    }
}
