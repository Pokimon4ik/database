﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1.Menu.Пункты
{
    public partial class LinkPoints : Form
    {
        public LinkPoints()
        {
            InitializeComponent();
        }

        public void RefLV()
        {
            lvPointTime.Items.Clear();

            using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
            {
                connection.Open();

                Dictionary<int, string> id = new Dictionary<int, string>();

                string sql = "SELECT * FROM [Пункт проката]";
                var command = connection.CreateCommand();
                command.CommandText = sql;
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        id.Add((int)reader["Код пункта"], reader["Адрес"].ToString());
                    }
                }

                sql = "SELECT * FROM [Время доставки]";
                command = connection.CreateCommand();
                command.CommandText = sql;
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var lvi = new ListViewItem(id[(int)reader["Код пункта назначения"]]);
                        lvi.SubItems.Add(id[(int)reader["Код пункта выезда"]]);
                        lvi.SubItems.Add(reader["Время доставки"].ToString());
                        lvPointTime.Items.Add(lvi);
                    }
                }
            }
        }

        private void NewLinkPoints_Load(object sender, EventArgs e)
        {
            RefLV();
        }

        private void NewLinkPoints_FormClosing(object sender, FormClosingEventArgs e)
        {
            Owner.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            NewLinkClass jojo = new NewLinkClass();
            jojo.Owner = this;
            jojo.Show();
        }
    }
}
