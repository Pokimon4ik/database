﻿namespace WindowsFormsApp1
{
    partial class FormPoints
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bond_artDataSet = new WindowsFormsApp1.bond_artDataSet();
            this.пункт_прокатаBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.пункт_прокатаTableAdapter = new WindowsFormsApp1.bond_artDataSetTableAdapters.Пункт_прокатаTableAdapter();
            this.tableAdapterManager = new WindowsFormsApp1.bond_artDataSetTableAdapters.TableAdapterManager();
            this.lvPoints = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Add = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnDelete1 = new WindowsFormsApp1.BtnDelete();
            this.btnAddLink = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.bond_artDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.пункт_прокатаBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // bond_artDataSet
            // 
            this.bond_artDataSet.DataSetName = "bond_artDataSet";
            this.bond_artDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // пункт_прокатаBindingSource
            // 
            this.пункт_прокатаBindingSource.DataMember = "Пункт проката";
            this.пункт_прокатаBindingSource.DataSource = this.bond_artDataSet;
            // 
            // пункт_прокатаTableAdapter
            // 
            this.пункт_прокатаTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.UpdateOrder = WindowsFormsApp1.bond_artDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.Бланк_регистрацииTableAdapter = null;
            this.tableAdapterManager.Время_доставкиTableAdapter = null;
            this.tableAdapterManager.КлиентTableAdapter = null;
            this.tableAdapterManager.Модификации_ОбъектаTableAdapter = null;
            this.tableAdapterManager.МодификацияTableAdapter = null;
            this.tableAdapterManager.ОбъектTableAdapter = null;
            this.tableAdapterManager.Пункт_прокатаTableAdapter = this.пункт_прокатаTableAdapter;
            // 
            // lvPoints
            // 
            this.lvPoints.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lvPoints.Location = new System.Drawing.Point(12, 12);
            this.lvPoints.Name = "lvPoints";
            this.lvPoints.Size = new System.Drawing.Size(201, 172);
            this.lvPoints.TabIndex = 0;
            this.lvPoints.UseCompatibleStateImageBehavior = false;
            this.lvPoints.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Код пункта";
            this.columnHeader1.Width = 70;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Адрес";
            this.columnHeader2.Width = 75;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Телефон";
            // 
            // Add
            // 
            this.Add.Location = new System.Drawing.Point(148, 217);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(65, 23);
            this.Add.TabIndex = 2;
            this.Add.Text = "Добавить";
            this.Add.UseVisualStyleBackColor = true;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(91, 217);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(51, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Выход";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnDelete1
            // 
            this.btnDelete1.Location = new System.Drawing.Point(8, 214);
            this.btnDelete1.Name = "btnDelete1";
            this.btnDelete1.Size = new System.Drawing.Size(84, 31);
            this.btnDelete1.TabIndex = 1;
            // 
            // btnAddLink
            // 
            this.btnAddLink.Location = new System.Drawing.Point(12, 190);
            this.btnAddLink.Name = "btnAddLink";
            this.btnAddLink.Size = new System.Drawing.Size(201, 23);
            this.btnAddLink.TabIndex = 4;
            this.btnAddLink.Text = "Cвязь пунктов";
            this.btnAddLink.UseVisualStyleBackColor = true;
            this.btnAddLink.Click += new System.EventHandler(this.btnAddLink_Click);
            // 
            // FormPoints
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(225, 252);
            this.Controls.Add(this.btnAddLink);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Add);
            this.Controls.Add(this.btnDelete1);
            this.Controls.Add(this.lvPoints);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormPoints";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Пункты";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormPoints_FormClosed);
            this.Load += new System.EventHandler(this.FormPoints_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bond_artDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.пункт_прокатаBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private bond_artDataSet bond_artDataSet;
        private System.Windows.Forms.BindingSource пункт_прокатаBindingSource;
        private bond_artDataSetTableAdapters.Пункт_прокатаTableAdapter пункт_прокатаTableAdapter;
        private bond_artDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private BtnDelete btnDelete1;
        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.ListView lvPoints;
        private System.Windows.Forms.Button btnAddLink;
    }
}