﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class NewPoint : Form
    {
        public NewPoint()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            FormPoints foo = this.Owner as FormPoints;

            using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
            {
                connection.Open();
                #region Добавление в объекты
                string sqlExpression = "INSERT INTO [Пункт проката] (Адрес, Телефон) VALUES (@Adres,@Telefon)";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                SqlParameter Adres = new SqlParameter("@Adres", tbAdres.Text);
                command.Parameters.Add(Adres);

                SqlParameter Telefon = new SqlParameter("@Telefon", tbTelefon.Text);
                command.Parameters.Add(Telefon);

                command.ExecuteNonQuery();
                #endregion
                #region Обновление таблици
                foo.lvPoints.Items.Clear();

                string sql = "SELECT * FROM [Пункт проката]";

                command = connection.CreateCommand();

                command.CommandText = sql;

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var lvi = new ListViewItem(reader["Код пункта"].ToString());
                        lvi.SubItems.Add(reader["Адрес"].ToString());
                        lvi.SubItems.Add(reader["Телефон"].ToString());
                        foo.lvPoints.Items.Add(lvi);
                    }
                }
                #endregion
            }
            Close();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void NewPoint_Load(object sender, EventArgs e)
        {

        }
    }
}
