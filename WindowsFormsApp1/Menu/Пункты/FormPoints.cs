﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Menu.Пункты;

namespace WindowsFormsApp1
{
    public partial class FormPoints : Form
    {
        public FormPoints()
        {
            InitializeComponent();
        }

        private void пункт_прокатаBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.пункт_прокатаBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.bond_artDataSet);

        }

        private void FormPoints_Load(object sender, EventArgs e)
        {
            string sql = "SELECT * FROM [Пункт проката]";
            using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
            {
                connection.Open();
                var command = connection.CreateCommand();

                command.CommandText = sql;

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var lvi = new ListViewItem(reader["Код пункта"].ToString());
                        lvi.SubItems.Add(reader["Адрес"].ToString());
                        lvi.SubItems.Add(reader["Телефон"].ToString());
                        lvPoints.Items.Add(lvi);
                    }
                }
            }
        }

        private void FormPoints_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Add_Click(object sender, EventArgs e)
        {
            NewPoint jojo = new NewPoint();
            jojo.Owner = this;
            jojo.Show();
        }

        private void btnAddLink_Click(object sender, EventArgs e)
        {
            LinkPoints jojo = new LinkPoints();
            jojo.Owner = this;
            Hide(); 
            jojo.Show();
        }
    }
}
