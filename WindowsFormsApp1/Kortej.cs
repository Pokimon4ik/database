﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class Kortej
    {
        string[] citys;

        public Kortej(List<(string, string, long)> graph)
        {
            bool permission_start = true;
            bool permission_final = true;
            int count = 0;
            citys = new string[graph.Count];
            foreach ((string, string, long) i in graph)
            {
                foreach (string j in citys)
                {
                    if (i.Item1 == j)
                        permission_start = false;
                    if (i.Item2 == j)
                        permission_final = false;
                }
                if (permission_start)
                {
                    citys[count] = i.Item1;
                    count++;
                }
                if (permission_final)
                {
                    citys[count] = i.Item2;
                    count++;
                }
                permission_start = true;
                permission_final = true;
            }
            citys = citys.Where(x => x != null).ToArray();
            Array.Sort(citys);
        }

        public long SmallDistanceTwoPoints(List<(string, string, long)> graph, string StartCity, string FinalCity)
        {
            List<(string, string, long)> road = new List<(string, string, long)>(graph);
            List<(string vertex, long value, bool visit)> bypass = new List<(string vertex, long value, bool visit)>();

            foreach (var i in citys)
            {
                bypass.Add((i, long.MaxValue, false));
            }

            var nowCity = StartCity;

            var index = bypass.FindIndex(x => x.vertex == nowCity);
            (string vertex, long value, bool visit) temp = (bypass[index].vertex, 0, bypass[index].visit);
            bypass[index] = temp;

            while (true)
            {
                var ways = road.FindAll(x => x.Item1 == nowCity);
                ways.RemoveAll(x => x.Item2 == bypass[bypass.FindIndex(z => z.vertex == x.Item2)].vertex && bypass[bypass.FindIndex(z => z.vertex == x.Item2)].visit);
                foreach (var i in ways)
                {
                    index = bypass.FindIndex(x => x.vertex == i.Item2);
                    temp = (i.Item2, i.Item3 + bypass[bypass.FindIndex(x => x.vertex == nowCity)].value, false);
                    if (bypass[index].value > temp.value)
                        bypass[index] = temp;
                }

                index = bypass.FindIndex(x => x.vertex == nowCity);
                if (index == -1)
                {
                    break;
                }
                temp = (nowCity, bypass[index].value, true);
                bypass[index] = temp;

                var sortedUsers = from u in bypass
                                  orderby u.value
                                  select u;
                bypass = sortedUsers.ToList();

                nowCity = bypass.Find(x => !x.visit).vertex;
            }
            return bypass.Find(x => x.vertex == FinalCity).value;
        }
    }
}
