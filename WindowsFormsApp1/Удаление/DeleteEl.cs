﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Menu.Пункты;

namespace WindowsFormsApp1
{
    public partial class DeleteEl : Form
    {
        public DeleteEl()
        {
            InitializeComponent();
        }

        Dictionary<int, string> clietns = new Dictionary<int, string>();

        private void DeleteEl_Load(object sender, EventArgs e)
        {
            
            if (Owner.Text == "Связи между пунктами выдачи")
            {
                using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
                {
                    connection.Open();
                    string sql = "SELECT * FROM [Время доставки] INNER JOIN [Пункт проката] ON [Время доставки].[Код пункта выезда] = [Пункт проката].[Код пункта]";
                    var command = connection.CreateCommand();
                    command.CommandText = sql;
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            cbIndex.Items.Add(reader["Адрес"] + " - " + reader["ID"]);
                        }
                    }
                }
            }

            if (Owner.Text == "Модификации")
            {
                string sql = "SELECT * FROM Модификация";
                using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
                {
                    connection.Open();
                    var command = connection.CreateCommand();

                    command.CommandText = sql;


                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            cbIndex.Items.Add(reader["Наименование модификации"] + " - " + reader["Код модификации"]);
                        }
                    }
                }
            }

            if (Owner.Text == "Пункты")
            {
                string sql = "SELECT * FROM [Пункт проката]";
                using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
                {
                    connection.Open();
                    var command = connection.CreateCommand();

                    command.CommandText = sql;


                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            cbIndex.Items.Add(reader["Адрес"] + " - " + reader["Код пункта"]);
                        }
                    }
                }
            }

            if (Owner.Text == "Клиенты")
            {
                string sql = "SELECT * FROM Клиент";
                using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
                {
                    connection.Open();
                    var command = connection.CreateCommand();

                    command.CommandText = sql;


                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            cbIndex.Items.Add(reader["ФИО"] + " - " + reader["Код клиента"]);
                        }
                    }
                }
            }

            if (Owner.Text == "Объекты")
            {
                string sql = "SELECT * FROM Объект";
                using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
                {
                    connection.Open();
                    var command = connection.CreateCommand();

                    command.CommandText = sql;


                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            cbIndex.Items.Add(reader["Наименование объекта"] + " - " + reader["Код объекта"]);
                        }
                    }
                }
            }

            if (Owner.Text == "Список клиентов арендующих объект")
            {
                string sql = "SELECT * FROM [Бланк регистрации] INNER JOIN Клиент ON Клиент.[Код клиента] = [Бланк регистрации].[Код клиента]";
                using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
                {
                    connection.Open();
                    var command = connection.CreateCommand();

                    command.CommandText = sql;


                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            cbIndex.Items.Add(reader["ФИО"] + " - " + reader["Код бланка"]);
                        }
                    }
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string sql = "";
            string ID = cbIndex.Text.Split(' ').Last();

            if (Owner.Text == "Клиенты")
            {
                sql = $"DELETE FROM Клиент WHERE [Код клиента] = '{ID}' ";
            }

            if (Owner.Text == "Объекты")
            {
                sql = $"DELETE FROM Объект WHERE [Код объекта] = '{ID}' ";
            }

            if (Owner.Text == "Список клиентов арендующих объект")
            {
                sql = $"DELETE FROM [Бланк регистрации] WHERE [Код бланка] = '{ID}' ";
            }

            if (Owner.Text == "Пункты")
            {
                sql = $"DELETE FROM [Пункт проката] WHERE [Код пункта] = '{ID}' ";
            }

            if (Owner.Text == "Модификации")
            {
                sql = $"DELETE FROM Модификация WHERE [Код модификации] = '{ID}' ";
            }

            if (Owner.Text == "Связи между пунктами выдачи")
            {
                sql = $"DELETE FROM [Время доставки] WHERE [ID] = '{ID}' ";
            }

            using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
            {
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = sql;
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (SqlException)
                {
                    MessageBox.Show("Данные присутствуют в других таблицах", "Ошибка", MessageBoxButtons.OK);
                }
            }

            if (Owner.Text == "Связи между пунктами выдачи")
            {
                LinkPoints jojo = (LinkPoints)Owner;
                jojo.RefLV();
            }

            if (Owner.Text == "Модификации")
            {
                FormMods jojo = (FormMods)Owner;
                jojo.RefreshVL();
            }

            if (Owner.Text == "Пункты")
            {
                FormPoints jojo = (FormPoints)Owner;
                jojo.lvPoints.Items.Clear();

                sql = "SELECT * FROM [Пункт проката]";
                using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
                {
                    connection.Open();
                    var command = connection.CreateCommand();

                    command.CommandText = sql;

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var lvi = new ListViewItem(reader["Код пункта"].ToString());
                            lvi.SubItems.Add(reader["Адрес"].ToString());
                            lvi.SubItems.Add(reader["Телефон"].ToString());
                            jojo.lvPoints.Items.Add(lvi);
                        }
                    }
                }
            }

            if (Owner.Text == "Клиенты")
            {
                FormClients jojo = (FormClients)Owner;
                jojo.lvClients.Items.Clear();

                sql = "SELECT * FROM Клиент";
                using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
                {
                    connection.Open();
                    var command = connection.CreateCommand();

                    command.CommandText = sql;

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var lvi = new ListViewItem(reader["Код клиента"].ToString());
                            lvi.SubItems.Add(reader["ФИО"].ToString());
                            lvi.SubItems.Add(reader["Адрес"].ToString());
                            lvi.SubItems.Add(reader["Телефон"].ToString());
                            jojo.lvClients.Items.Add(lvi);
                        }
                    }
                }
            }

            if (Owner.Text == "Объекты")
            {
                FormObjs jojo = (FormObjs)Owner;
                jojo.lvObjs.Items.Clear();

                sql = "SELECT * FROM Объект";
                using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
                {
                    connection.Open();
                    var command = connection.CreateCommand();

                    command.CommandText = sql;

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var lvi = new ListViewItem(reader["Код объекта"].ToString());
                            lvi.SubItems.Add(reader["Наименование объекта"].ToString());
                            lvi.SubItems.Add(reader["Тип объекта"].ToString());
                            lvi.SubItems.Add(reader["Вес"].ToString());
                            lvi.SubItems.Add(reader["Цена"].ToString());
                            jojo.lvObjs.Items.Add(lvi);
                        }
                    }
                }
            }

            if (Owner.Text == "Список клиентов арендующих объект")
            {
                FormBlank jojo = (FormBlank)Owner;
                jojo.lvBlank.Items.Clear();

                sql = "SELECT * FROM [Бланк регистрации] " +
                "INNER JOIN Клиент ON Клиент.[Код клиента] = [Бланк регистрации].[Код клиента] " +
                "INNER JOIN [Пункт проката] ON [Пункт проката].[Код пункта] = [Бланк регистрации].[Код пункта] " +
                "INNER JOIN Объект ON Объект.[Код объекта] = [Бланк регистрации].[Код объекта]";
                using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.bond_artConnectionString))
                {
                    connection.Open();
                    var command = connection.CreateCommand();

                    command.CommandText = sql;

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var lvi = new ListViewItem(reader["ФИО"].ToString());
                            lvi.SubItems.Add(reader["Цена аренды"].ToString());
                            lvi.SubItems.Add(reader["Наименование объекта"].ToString());
                            lvi.SubItems.Add(reader["Адрес"].ToString());
                            lvi.SubItems.Add(reader["Время начала аренды"].ToString());
                            lvi.SubItems.Add(reader["Время конца аренды"].ToString());
                            jojo.lvBlank.Items.Add(lvi);
                        }
                    }
                }
            }
        }
    }
}
