﻿namespace WindowsFormsApp1
{
    partial class DeleteEl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnDelete = new System.Windows.Forms.Button();
            this.label = new System.Windows.Forms.Label();
            this.cbIndex = new System.Windows.Forms.ComboBox();
            this.bond_artDataSet = new WindowsFormsApp1.bond_artDataSet();
            this.времяДоставкиBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.время_доставкиTableAdapter = new WindowsFormsApp1.bond_artDataSetTableAdapters.Время_доставкиTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.bond_artDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.времяДоставкиBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(146, 23);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 3;
            this.btnDelete.Text = "Удалить";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(12, 9);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(88, 13);
            this.label.TabIndex = 5;
            this.label.Text = "Введите индекс";
            // 
            // cbIndex
            // 
            this.cbIndex.FormattingEnabled = true;
            this.cbIndex.Location = new System.Drawing.Point(15, 25);
            this.cbIndex.Name = "cbIndex";
            this.cbIndex.Size = new System.Drawing.Size(125, 21);
            this.cbIndex.TabIndex = 6;
            // 
            // bond_artDataSet
            // 
            this.bond_artDataSet.DataSetName = "bond_artDataSet";
            this.bond_artDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // времяДоставкиBindingSource
            // 
            this.времяДоставкиBindingSource.DataMember = "Время доставки";
            this.времяДоставкиBindingSource.DataSource = this.bond_artDataSet;
            // 
            // время_доставкиTableAdapter
            // 
            this.время_доставкиTableAdapter.ClearBeforeFill = true;
            // 
            // DeleteEl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(233, 55);
            this.Controls.Add(this.cbIndex);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.label);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "DeleteEl";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Удаление";
            this.Load += new System.EventHandler(this.DeleteEl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bond_artDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.времяДоставкиBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.ComboBox cbIndex;
        private bond_artDataSet bond_artDataSet;
        private System.Windows.Forms.BindingSource времяДоставкиBindingSource;
        private bond_artDataSetTableAdapters.Время_доставкиTableAdapter время_доставкиTableAdapter;
    }
}